# ASP projekt



## Description
Ovo je Moj projekt iz ASPa. 
Za projekt sam imao BitcoinHeist Ransomware dataset


## Rezultati
Budući da mi trebaju 3 različita stupca s kojima radim po konceptu ključ-vrijednost, odlučio sam koristiti **multimap**
zbog toga što je već podijeljen u key-value parove, a uz to također ima poželjne kompleksnosti za ono što mi treba.


To se skoro pokazalo istinitim, osim što se brisanje podataka pokazalo poprilično kompleksnim, budući da koristim 3 multimapa s različitim ključevima, a moram ih uskladiti.
Rezultati testiranja su:

* Dohvaćanje Min i Max vrijednosti se odvija brzinama ispod 5ms što je i očekivano jer je predviđena kompleksnost konstanta.
* Dodavanje novih članova se odvija brzinama od 0-15ms što je također očekivano. Predviđena kompleksnost je log(n), što je ostvareno.

* Pretraživanje se odvija brzinama od 0-10ms. Kompleksnost pretraživanja je također log(n).

* Kod brisanja dolazi do problema. Da brišem samo u jednom multimapu, kompleksnost brisanja bi bila log(n). Nažalost moram obrisat u 3 multimapa, i moram pazit na to da slučajno ne obrišem više od onoga što sam trebao. Za prosječno brisanje zbog toga moram
- pronaći jednake podatke u sva 3 multimapa s valjanim kljućevima (log n)
- budući da samo jedan kljuć ima unikatne vrijednosti, usporediti dobivene vrijednosti dok ne dobijem potpuno ispravni trojac podataka(n)
- izbrisati same podatke(log n)
- Ovo je poprilično loša kompleksnost pogotovo jer brišem sve podatke tražene vrijednosti (u slučaju s neunikatnim ključevima), što se da isčitati iz samih rezultata testiranja (10 000-100 000ms) u slučaju normalnog broja podataka s istim vrijednostima. u slučaju gdje je Income ili Weight 1, broj članova je u desetcima tisuća, i vrijeme potrebno da se izvrši potpuni delete svih tih vrijednosti je u trajanju od više od 10 minuta.
