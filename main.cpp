/* Podatci:
address - string
year-int
day-int
length-int
weight-float
count-int
looped-int
neighbors-int
income-int
label-string
*/

// includeovi
#include <chrono>  // za mjerenje proteklog vremena
#include <fstream> //za čitanje fileova
#include <iostream>
#include <map> //pohranjivanje u multimap - U zadatku se navode key-value parovi->multimap koristi key value parove, sam se sortira i može obaviti tražene stvari u zadovoljavajućoj kompleksnosti
#include <vector> //Ukoliko želim vratiti n vrijednosti, gdje je svaki n tipa BHeist trebat ce mi nesto u sta to mogu spremiti, vector ima najbrzu kompleksnost dodavanja podataka(konstantnu), a za svrhe ovog projekta moram iskljucivo vracati podatke, te onda nista zapravno ne raditi s njima, za sto je vektor zadovoljavajuc
using namespace std::chrono;

struct BHeist {
  std::string address;
  int y;
  int d;
  int l;
  // brojevi su malo preveliki za float
  double w;
  // brojevi su malo preveliki za obicni int
  unsigned int count;
  int loop;
  int neighbors;
  int income;
  std::string label;
  // ovo mi treba za brisanje
  bool operator==(BHeist &b) {
    if (address == b.address && y == b.y && d == b.d && l == b.l && w == b.w &&
        count == b.count && loop == b.loop && neighbors == b.neighbors &&
        income == b.income && label == b.label) {
      return true;
    } else
      return false;
  };
};

class Heistdb {
private:
  // odlucio sam se za adresu, weight i income, jer je svaki razlicitog tipa
  std::multimap<std::string, BHeist> KeyAddress;
  std::multimap<float, BHeist> KeyWeight;
  std::multimap<int, BHeist> KeyIncome;

public:
  // Konstruktor
  Heistdb() {
    std::fstream file("BitcoinHeistData.csv", std::ios::in);
    int i = 0;
    if (file.is_open()) {

      // CSV ima previse podataka da mogu ucitati cijeli CSV, ucitat cu samo
      // 500 000 kao sto se trazi
      // moramo preskocit prvi red jer on ne sadrzi prave podatke
      std::string currentLine;

      bool newLine = true;
      while (getline(file, currentLine)) {
        if (i >= 500000) {
          break;
        }
        if (newLine) {
          newLine = false;
        } else {
          // substr uzima "tekst" izmedu zareza pomocu pocetne tocke i duljine
          // teksta odvajac koristim da se odvojim podatke i da se rijesim
          // zareza odvajac +1 osigurava da slucajno "ne pojedem" zarez, to jest
          // da ga ne procitam i stavim u neku varijablu zatim trazim sljedeci
          // zarez da dobijem stvarnu duljinu varijable, moram oduzeti odvajac i
          // jos jedno mjesto stoi i stof pretvaraju tekst u brojeve
          BHeist Heist;
          Heist.address = currentLine.substr(0, currentLine.find(',', 0));
          int odvajac = currentLine.find(',', 0);
          Heist.y = stoi(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.d = stoi(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.l = stoi(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.w = stod(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.count = stoi(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.loop = stoi(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.neighbors = stoi(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.income = stol(currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1));
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;
          Heist.label = currentLine.substr(
              odvajac + 1, currentLine.find(',', odvajac + 1) - odvajac - 1);
          odvajac =
              odvajac + 1 + currentLine.find(',', odvajac + 1) - odvajac - 1;

          KeyAddress.insert({Heist.address, Heist});
          KeyWeight.insert({Heist.w, Heist});
          KeyIncome.insert({Heist.income, Heist});
          i++;
        }
      }
    }

    file.close();
  }
  // Prvo cu obavit sve funkcije za Adrese

  // Vraca najvecu adresu, rbegin() je konstantne kompleksnosti
  std::string ReturnMaxAddress1a() {
    std::cout << "Vracam najvecu adresu" << std::endl;
    auto start = high_resolution_clock::now();
    std::string a = KeyAddress.rbegin()->first;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca Bheist na najvecoj adresi, rbegin() je konstantne kompleksnosti
  BHeist ReturnMaxAddress1b() {
    std::cout << "Vracam vrijednost iza najvece adrese";
    auto start = high_resolution_clock::now();
    BHeist a = KeyAddress.rbegin()->second;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca prvih n najvecih adresa, buduci da idem od rbegina() do broja
  // n,kompleksnost bi trebala biti n spremam prvih n adresa u vektor, i ide do
  // broja n, a j je iterator kroz strukturu Bitcoin Heistova
  std::vector<std::string> ReturnMaxAddress2a(int n) {
    std::cout << "Vracam " << n << " najvecih adresa" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyAddress.rbegin();
    std::vector<std::string> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->first);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca vrijednosti prvih n najvecih adresa, buduci da idem od rbegina() do
  // broja n,kompleksnost bi trebala biti n spremam prvih n adresa u vektor, i
  // ide do broja n, a j je iterator kroz strukturu Bitcoin Heistova
  std::vector<BHeist> ReturnMaxAddress2b(int n) {
    std::cout << "Vracam vrijednosti " << n << " najvecih adresa" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyAddress.rbegin();
    std::vector<BHeist> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->second);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca najmanju adresu, begin() je konstantne kompleksnosti
  std::string ReturnMinAddress1a() {
    std::cout << "Vracam najmanju adresu" << std::endl;
    auto start = high_resolution_clock::now();
    std::string a = KeyAddress.begin()->first;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca BHeist na najmanjoj adresi, begin() je konstantne kompleksnosti
  BHeist ReturnMinAddress1b() {
    std::cout << "Vracam vrijednost iza najmanje adrese";
    auto start = high_resolution_clock::now();
    BHeist a = KeyAddress.begin()->second;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca prvih n najmanjih adresa, buduci da idem od begina() do broja
  // n,kompleksnost bi trebala biti n spremam prvih n adresa u vektor, i ide do
  // broja n, a j je iterator kroz strukturu Bitcoin Heistova
  std::vector<std::string> ReturnMinAddress2a(int n) {
    std::cout << "Vracam " << n << " najmanjih adresa" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyAddress.begin();
    std::vector<std::string> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->first);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca vrijednosti prvih n najmanjih adresa, buduci da idem od begina() do
  // broja n,kompleksnost bi trebala biti n spremam prvih n adresa u vektor, i
  // ide do broja n, a j je iterator kroz strukturu Bitcoin Heistova
  std::vector<BHeist> ReturnMinAddress2b(int n) {
    std::cout << "Vracam vrijednosti " << n << " najmanjih adresa" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyAddress.begin();
    std::vector<BHeist> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->second);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Dodaje Heist po adresi, adrese moraju biti unikatne
  // Ukoliko su adrese unikatne, kompleksnost je log(n), jedan za provjeru,
  // ostali za insert
  void AddByAddress(std::string a, BHeist h) {
    std::cout << "Pokušavam dodati clana" << std::endl;
    auto start = high_resolution_clock::now();
    h.address = a;
    if (KeyAddress.count(a) == 0) {
      KeyAddress.insert({h.address, h});
      KeyWeight.insert({h.w, h});
      KeyIncome.insert({h.income, h});
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Dodaje Heistove po adresama, adrese moraju biti unikatne
  // Ukoliko su adrese unikatne, kompleksnost je mlog(n), jedan za provjeru,
  // ostali za insert
  // m je broj Heistova koji se treba unijeti
  // adresa mora biti manje ili jednako nego Heistova
  void AddByAddresses(std::vector<std::string> a, std::vector<BHeist> h) {
    std::cout << "Pokušavam dodati clanove" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < a.size(); i++) {
      h[i].address = a[i];
      if (KeyAddress.count(a[i]) == 0) {
        KeyAddress.insert({h[i].address, h[i]});
        KeyWeight.insert({h[i].w, h[i]});
        KeyIncome.insert({h[i].income, h[i]});
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Ovo ne moram ponavljati i za weight i income
  // Kompleksnost log(n)
  void AddByValue(BHeist h) {
    std::cout << "Pokušavam dodati clana" << std::endl;
    auto start = high_resolution_clock::now();
    if (KeyAddress.count(h.address) == 0) {
      KeyAddress.insert({h.address, h});
      KeyWeight.insert({h.w, h});
      KeyIncome.insert({h.income, h});
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  void AddByValues(std::vector<BHeist> h) {
    std::cout << "Pokušavam dodati clana" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i <= h.size()-1; i++)
      if (KeyAddress.count(h[i].address) == 0) {
        KeyAddress.insert({h[i].address, h[i]});
        KeyWeight.insert({h[i].w, h[i]});
        KeyIncome.insert({h[i].income, h[i]});
      }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }

  // Search
  // kompleksnost log(n) zbog find()
  bool searchAddress1(std::string a) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyAddress.find(a);
    if (search == KeyAddress.end()) {
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      std::cout << "Proces obavljen u " << duration.count() << "ms, nije uspio"
                << std::endl;
      return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return true;
  }
  // kompleksnost m*(log(n))
  // m je broj pretraga
  void searchAddress2(std::vector<std::string> a) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i <a.size(); i++) {
      auto search = KeyAddress.find(a[i]);
      if (search == KeyAddress.end()) {
        std::cout << "Vrijednost s adresom" << a[i] << "nije pronadena"
                  << std::endl;
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Search po vrijednosti
  // Kompleksnost  log(n)
  bool searchAddressValue(BHeist h) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyAddress.find(h.address);
    if (search == KeyAddress.end()) {
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      std::cout << "Proces obavljen u " << duration.count() << "ms"
                << std::endl;
      return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return true;
  }
  // Search po više vrijednost
  // Kompleksnost  mlog(n)
  // m je broj pretraga
  void searchAddressValues(std::vector<BHeist> h) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < h.size(); i++) {
      auto search = KeyAddress.find(h[i].address);
      if (search == KeyAddress.end()) {
        std::cout << "Vrijednost s vrijednostima" << h[i].address
                  << "nije pronadena" << std::endl;
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Delete
  // briše sve elemente s nekom adresom
  // kompleknost je trebala biti m*log(n), no puno je veća
  void deleteAddress(std::string a) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyAddress.find(a);
    while (search != KeyAddress.end()) {
      if(search==KeyAddress.end()){
        break;
      }
      // moram obrisat u ostalim strukturama
      // stoga potražim istu vrijednost u ostalim strukturama kako bi ju mogao
      // obrisati. Nažalost moguce je napravit gresku jer moze postojati vise
      // podataka s istim weightom/incomeom, zbog toga treba ic kroz njih dok se
      // ne dode do onog s tocnom adresom. to podosta pojacava kompleksnost,
      // doduse ne znam kak poravnat sve 3 strukture, a ukoliko zelim izac na
      // zimski rok moram se pomirit s ovim
      auto delw = KeyWeight.find(search->second.w);
      while (true) {
        if (search->second == delw->second) {
          break;
        }
        delw++;
      }
      auto deli = KeyIncome.find(search->second.income);
      while (true) {
        if (search->second == deli->second) {
          break;
        }
        deli++;
      }
      KeyAddress.erase(search);
      KeyWeight.erase(delw);
      KeyIncome.erase(deli);
      search = KeyAddress.find(a);
    };
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // briše sve elemente s adresama navedenima u vektoru
  // kompleknost je trebala biti x*y*log(n), no puno je veća
  void deleteAddresses(std::vector<std::string> a) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < a.size(); i++) {
      auto search = KeyAddress.find(a[i]);
      while (search != KeyAddress.end()) {
        auto delw = KeyWeight.find(search->second.w);
        while (true) {
          if (search->second == delw->second) {
            break;
          }
          delw++;
        }
        auto deli = KeyIncome.find(search->second.income);
        while (true) {
          if (search->second == deli->second) {
            break;
          }
          deli++;
        }
        KeyAddress.erase(search);
        KeyWeight.erase(delw);
        KeyIncome.erase(deli);
        search = KeyAddress.find(a[i]);
      };
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Briše sve elemente jednake Heistu H
  // kompleknost je trebala biti m*log(n), no puno je veća
  void DeleteValue(BHeist h) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyAddress.find(h.address);
    auto delw = KeyWeight.find(h.w);
    while (true) {
      if (h == delw->second) {
        break;
      }
      delw++;
    }
    auto deli = KeyIncome.find(h.income);
    while (true) {
      if (h == deli->second) {
        break;
      }
      deli++;
    }
    KeyAddress.erase(search);
    KeyWeight.erase(delw);
    KeyIncome.erase(deli);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Briše sve elemente jednake Heistovima u vektoru
  // kompleknost je trebala biti x*y*log(n), no puno je veća
  void DeleteValues(std::vector<BHeist> h) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < h.size(); i++) {
      auto search = KeyAddress.find(h[i].address);
      auto delw = KeyWeight.find(h[i].w);
      while (true) {
        if (h[i] == delw->second) {
          break;
        }
        delw++;
      }
      auto deli = KeyIncome.find(h[i].income);
      while (true) {
        if (h[i] == deli->second) {
          break;
        }
        deli++;
      }
      KeyAddress.erase(search);
      KeyWeight.erase(delw);
      KeyIncome.erase(deli);
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }

  // Sve funkcije za weight
  // Vraća najveću težinu, konstantna kompleksnost
  double ReturnMaxWeight1a() {
    std::cout << "Vracam najvecu težinu" << std::endl;
    auto start = high_resolution_clock::now();
    double a = KeyWeight.rbegin()->first;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraća Heist s najvećom težinom, konstantna kompleksnost
  BHeist ReturnMaxWeight1b() {
    std::cout << "Vracam vrijednost iza najvece težine";
    auto start = high_resolution_clock::now();
    BHeist a = KeyWeight.rbegin()->second;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraća prvih n težina, kompleksnost n
  std::vector<double> ReturnMaxWeight2a(int n) {
    std::cout << "Vracam " << n << " najvecih težina" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyWeight.rbegin();
    std::vector<double> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->first);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // vraća prvih n Heistova s najvećim težinama, kompleksnost n
  std::vector<BHeist> ReturnMaxWeight2b(int n) {
    std::cout << "Vracam vrijednosti " << n << " najvecih težina" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyWeight.rbegin();
    std::vector<BHeist> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->second);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // vraća najmanju težinu, konstantna kompleksnost
  double ReturnMinWeight1a() {
    std::cout << "Vracam najmanju težinu" << std::endl;
    auto start = high_resolution_clock::now();
    double a = KeyWeight.begin()->first;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca BHeist na najmanjoj težini, begin() je konstantne kompleksnosti
  BHeist ReturnMinWeight1b() {
    std::cout << "Vracam vrijednost iza najmanje težine";
    auto start = high_resolution_clock::now();
    BHeist a = KeyWeight.begin()->second;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca prvih n najmanjih težina, buduci da idem od begina() do broja
  // n,kompleksnost bi trebala biti n spremam prvih n težina u vektor, i ide do
  // broja n, a j je iterator kroz strukturu Bitcoin Heistova
  std::vector<double> ReturnMinWeight2a(int n) {
    std::cout << "Vracam " << n << " najmanjih težina" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyWeight.begin();
    std::vector<double> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->first);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca vrijednosti prvih n najmanjih težina
  std::vector<BHeist> ReturnMinWeight2b(int n) {
    std::cout << "Vracam vrijednosti " << n << " najmanjih težina" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyWeight.begin();
    std::vector<BHeist> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->second);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Dodaje Heist po težini, adrese moraju biti unikatne
  // Ukoliko su adrese unikatne, kompleksnost je log(n), jedan za provjeru,
  // ostali za insert
  void AddByWeight(double a, BHeist h) {
    std::cout << "Pokušavam dodati clana" << std::endl;
    auto start = high_resolution_clock::now();
    h.w = a;
    if (KeyAddress.count(h.address) == 0) {
      KeyAddress.insert({h.address, h});
      KeyWeight.insert({a, h});
      KeyIncome.insert({h.income, h});
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Dodaje Heistove po težinama, adrese moraju biti unikatne
  // Ukoliko su adrese unikatne, kompleksnost je mlog(n), jedan za provjeru,
  // ostali za insert
  // m je broj Heistova koji se treba unijeti
  // adresa mora biti manje ili jednako nego Heistova
  void AddByWeights(std::vector<double> a, std::vector<BHeist> h) {
    std::cout << "Pokušavam dodati clanove" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < a.size(); i++) {
      h[i].w = a[i];
      if (KeyAddress.count(h[i].address) == 0) {
        KeyAddress.insert({h[i].address, h[i]});
        KeyWeight.insert({a[i], h[i]});
        KeyIncome.insert({h[i].income, h[i]});
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Searchovi
  // Kompleksnost log(n)
  bool searchWeight1(double a) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyWeight.find(a);
    if (search == KeyWeight.end()) {
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      std::cout << "Proces obavljen u " << duration.count() << "ms, nije uspio"
                << std::endl;
      return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return true;
  }
  // Kompleksnost m*log(n)
  // m je broj pretraga
  void searchWeight2(std::vector<double> a) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < a.size(); i++) {
      auto search = KeyWeight.find(a[i]);
      if (search == KeyWeight.end()) {
        std::cout << "Vrijednost s adresom" << a[i] << "nije pronadena"
                  << std::endl;
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // pretraživanje po vrijednosti
  bool searchWeightValue(BHeist h) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyWeight.find(h.w);
    if (search == KeyWeight.end()) {
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      std::cout << "Proces obavljen u " << duration.count() << "ms"
                << std::endl;
      return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return true;
  }
  // Search po više vrijednost
  // Kompleksnost  mlog(n)
  // m je broj pretraga
  void searchWeightValues(std::vector<BHeist> h) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < h.size(); i++) {
      auto search = KeyWeight.find(h[i].w);
      if (search == KeyWeight.end()) {
        std::cout << "Vrijednost s vrijednostima" << h[i].address
                  << "nije pronadena" << std::endl;
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Brisanje
  // briše sve elemente s navedenom težinom
  void deleteWeight(double a) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyWeight.find(a);
    while (search != KeyWeight.end()) {
      if(search==KeyWeight.end()){
        break;
      }
      auto dela = KeyAddress.find(search->second.address);
      while (true) {
        if (search->second == dela->second) {
          break;
        }
        dela++;
      }
      auto deli = KeyIncome.find(search->second.income);
      while (true) {
        if (search->second == deli->second) {
          break;
        }
        deli++;
      }
      KeyAddress.erase(dela);
      KeyWeight.erase(search);
      KeyIncome.erase(deli);
      search = KeyWeight.find(a);
    };
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // briše sve elemente s težinama navedenima u vektoru
  // kompleknost je trebala biti x*y*log(n), no puno je veća
  void deleteWeights(std::vector<double> a) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < a.size(); i++) {
      std::cout<<"Pokušavam"<<std::endl;
      auto search = KeyWeight.find(a[i]);
      while (search != KeyWeight.end()) {
        if(search==KeyWeight.end()){
        break;
      }
        auto dela = KeyAddress.find(search->second.address);
        while (true) {
          if (search->second == dela->second) {
            break;
          }
          dela++;
        }
        deleteAddress(dela->first);
        search = KeyWeight.find(a[i]);
      };
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Income

  // Max/Min
  // Vraća najveću težinu, konstantna kompleksnost
  int ReturnMaxIncome1a() {
    std::cout << "Vracam najveci prihod" << std::endl;
    auto start = high_resolution_clock::now();
    int a = KeyIncome.rbegin()->first;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraća Heist s najvećom težinom, konstantna kompleksnost
  BHeist ReturnMaxIncome1b() {
    std::cout << "Vracam vrijednost iza najveceg prihoda";
    auto start = high_resolution_clock::now();
    BHeist a = KeyIncome.rbegin()->second;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraća prvih n težina, kompleksnost n
  std::vector<int> ReturnMaxIncome2a(int n) {
    std::cout << "Vracam " << n << " najvecih prihoda" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyIncome.rbegin();
    std::vector<int> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->first);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // vraća prvih n Heistova s najvećim prihodima, kompleksnost n
  std::vector<BHeist> ReturnMaxIncome2b(int n) {
    std::cout << "Vracam vrijednosti " << n << " najvecih prihoda" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyIncome.rbegin();
    std::vector<BHeist> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->second);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // vraća najmanju težinu, konstantna kompleksnost
  int ReturnMinIncome1a() {
    std::cout << "Vracam najmanju težinu" << std::endl;
    auto start = high_resolution_clock::now();
    int a = KeyIncome.begin()->first;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca BHeist na najmanjem prihodu, begin() je konstantne kompleksnosti
  BHeist ReturnMinIncome1b() {
    std::cout << "Vracam vrijednost iza najmanje prihoda";
    auto start = high_resolution_clock::now();
    BHeist a = KeyIncome.begin()->second;
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca prvih n najmanjih prihoda, buduci da idem od begina() do broja
  // n,kompleksnost bi trebala biti n spremam prvih n prihoda u vektor, i ide do
  // broja n, a j je iterator kroz strukturu Bitcoin Heistova
  std::vector<int> ReturnMinIncome2a(int n) {
    std::cout << "Vracam " << n << " najmanjih prihoda" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyIncome.begin();
    std::vector<int> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->first);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
  // Vraca vrijednosti prvih n najmanjih težina
  std::vector<BHeist> ReturnMinIncome2b(int n) {
    std::cout << "Vracam vrijednosti " << n << " najmanjih težina" << std::endl;
    auto start = high_resolution_clock::now();
    auto j = KeyIncome.begin();
    std::vector<BHeist> a;
    for (int i = 0; i < n; i++) {
      a.push_back(j->second);
      j++;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return a;
  }
   // Dodaje Heist po prihodu, adrese moraju biti unikatne
  // Ukoliko su adrese unikatne, kompleksnost je log(n), jedan za provjeru,
  // ostali za insert
  void AddByIncome(int a, BHeist h) {
    std::cout << "Pokušavam dodati clana" << std::endl;
    auto start = high_resolution_clock::now();
    h.income = a;
    if (KeyAddress.count(h.address) == 0) {
      KeyAddress.insert({h.address, h});
      KeyWeight.insert({h.w, h});
      KeyIncome.insert({a, h});
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Dodaje Heistove po prihodima, adrese moraju biti unikatne
  // Ukoliko su adrese unikatne, kompleksnost je mlog(n), jedan za provjeru,
  // ostali za insert
  // m je broj Heistova koji se treba unijeti
  // adresa mora biti manje ili jednako nego Heistova
  void AddByIncomes(std::vector<int> a, std::vector<BHeist> h) {
    std::cout << "Pokušavam dodati clanove" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i <a.size(); i++) {
      h[i].income = a[i];
      if (KeyAddress.count(h[i].address) == 0) {
        KeyAddress.insert({h[i].address, h[i]});
        KeyWeight.insert({h[i].w, h[i]});
        KeyIncome.insert({a[i], h[i]});
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // Searchovi
  // Kompleksnost log(n)
  bool searchIncome1(int a) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyIncome.find(a);
    if (search == KeyIncome.end()) {
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      std::cout << "Proces obavljen u " << duration.count() << "ms, nije uspio"
                << std::endl;
      return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return true;
  }
  // Kompleksnost m*log(n)
  // m je broj pretraga
  void searchIncome2(std::vector<int> a) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i <a.size(); i++) {
      auto search = KeyIncome.find(a[i]);
      if (search == KeyIncome.end()) {
        std::cout << "Vrijednost s adresom" << a[i] << "nije pronadena"
                  << std::endl;
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // pretraživanje po vrijednosti
  bool searchIncomeValue(BHeist h) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyIncome.find(h.w);
    if (search == KeyIncome.end()) {
      auto stop = high_resolution_clock::now();
      auto duration = duration_cast<microseconds>(stop - start);
      std::cout << "Proces obavljen u " << duration.count() << "ms"
                << std::endl;
      return false;
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
    return true;
  }
  // Search po više vrijednost
  // Kompleksnost  mlog(n)
  // m je broj pretraga
  void searchIncomeValues(std::vector<BHeist> h) {
    std::cout << "Pokušavam pretragu" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < h.size(); i++) {
      auto search = KeyIncome.find(h[i].w);
      if (search == KeyIncome.end()) {
        std::cout << "Vrijednost s vrijednostima" << h[i].address
                  << "nije pronadena" << std::endl;
      }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
// Delete
  // briše sve elemente s nekim prihodom
  // kompleknost je trebala biti m*log(n), no puno je veća
  void deleteIncome(int a) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    auto search = KeyIncome.find(a);
    while (search != KeyIncome.end()) {
      if(search==KeyIncome.end()){
        break;
      }
      auto dela = KeyAddress.find(search->second.address);
      while (true) {
        if (search->second == dela->second) {
          break;
        }
        dela++;
      }
      auto delw = KeyWeight.find(search->second.w);
      while (true) {
        if (dela->second == delw->second) {
          
          break;
        }
        delw++;
      }
      
      KeyAddress.erase(dela);
      KeyWeight.erase(delw);
      KeyIncome.erase(search);
      search = KeyIncome.find(a);
    };
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
  // briše sve elemente s adresama navedenima u vektoru
  // kompleknost je trebala biti x*y*log(n), no puno je veća
  void deleteIncomes(std::vector<int> a) {
    std::cout << "Pokušavam brisanje" << std::endl;
    auto start = high_resolution_clock::now();
    for (int i = 0; i < a.size(); i++) {
      std::cout<<"Pokušavam"<<std::endl;
      auto search = KeyIncome.find(a[i]);
      while (search != KeyIncome.end()) {
        if(search==KeyIncome.end()){
          break;
        }
         auto dela = KeyAddress.find(search->second.address);
        while (true) {
          if (search->second == dela->second) {
            break;
          }
          dela++;
        }
        deleteAddress(dela->first);
        std::cout<<KeyWeight.count(1)<<std::endl;
        std::cout<<KeyIncome.count(1)<<std::endl;
        search=KeyIncome.find(a[i]);
      };
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    std::cout << "Proces obavljen u " << duration.count() << "ms" << std::endl;
  }
};

int main() {
  Heistdb baza;
  //Minovi/Maxovi
  std::cout << "Hello World!\n";
  std::cout<< baza.ReturnMaxAddress1a()<<std::endl;
  std::cout<<baza.ReturnMaxAddress1b().address<<std::endl;
  std::vector<std::string> a=baza.ReturnMaxAddress2a(5);
  for (int i=0;i<5;i++){
    std::cout<<a[i]<<std::endl;
  }
  std::vector<BHeist>b=baza.ReturnMaxAddress2b(5);
  for (int i=0;i<5;i++){
    std::cout<<b[i].address<<std::endl;
  }
  std::cout << "Hello World!\n";
  std::cout<< baza.ReturnMinAddress1a()<<std::endl;
  std::cout<<baza.ReturnMinAddress1b().address<<std::endl;
  std::vector<std::string> c=baza.ReturnMinAddress2a(5);
  for (int i=0;i<5;i++){
    std::cout<<c[i]<<std::endl;
  }
  std::vector<BHeist>d=baza.ReturnMinAddress2b(5);
  for (int i=0;i<5;i++){
    std::cout<<d[i].address<<std::endl;
  }
    std::cout<< baza.ReturnMaxWeight1a()<<std::endl;
  std::cout<<baza.ReturnMaxWeight1b().address<<std::endl;
  std::vector<double> e=baza.ReturnMaxWeight2a(5);
  for (int i=0;i<5;i++){
    std::cout<<e[i]<<std::endl;
  }
  std::vector<BHeist>f=baza.ReturnMaxWeight2b(5);
  for (int i=0;i<5;i++){
    std::cout<<f[i].w<<std::endl;
  }
  std::cout << "Hello World!\n";
  std::cout<< baza.ReturnMinWeight1a()<<std::endl;
  std::cout<<baza.ReturnMinWeight1b().w<<std::endl;
  std::vector<double> g=baza.ReturnMinWeight2a(5);
  for (int i=0;i<5;i++){
    std::cout<<g[i]<<std::endl;
  }
  std::vector<BHeist>h=baza.ReturnMinWeight2b(5);
  for (int i=0;i<5;i++){
    std::cout<<h[i].w<<std::endl;
  }
      std::cout<< baza.ReturnMaxIncome1a()<<std::endl;
  std::cout<<baza.ReturnMaxIncome1b().address<<std::endl;
  std::vector<int> j=baza.ReturnMaxIncome2a(5);
  for (int i=0;i<5;i++){
    std::cout<<j[i]<<std::endl;
  }
  std::vector<BHeist>k=baza.ReturnMaxIncome2b(5);
  for (int i=0;i<5;i++){
    std::cout<<k[i].w<<std::endl;
  }
  std::cout << "Hello World!\n";
  std::cout<< baza.ReturnMinIncome1a()<<std::endl;
  std::cout<<baza.ReturnMinIncome1b().w<<std::endl;
  std::vector<int> l=baza.ReturnMinIncome2a(5);
  for (int i=0;i<5;i++){
    std::cout<<l[i]<<std::endl;
  }
  std::vector<BHeist>m=baza.ReturnMinIncome2b(5);
  for (int i=0;i<5;i++){
    std::cout<<m[i].w<<std::endl;
  }
  //ADD
  BHeist atest={"atest",1,1,1,1.25,1,1,1,1,"test"};
  BHeist wtest={"wtest",1,1,1,1.25,1,1,1,1,"test"};
  BHeist itest={"atest",1,1,1,1.25,1,1,1,1,"test"};
  BHeist vtest={"vtest",1,1,1,1.25,1,1,1,1,"test"};
  BHeist atest1={"atest1",1,1,1,1.25,1,1,1,1,"test"};
  BHeist atest2={"atest2",1,1,1,1.25,1,1,1,1,"test"};
  BHeist wtest1={"wtest1",1,1,1,1.25,1,1,1,1,"test"};
  BHeist wtest2={"wtest2",1,1,1,1.25,1,1,1,1,"test"};
  BHeist itest1={"atest1",1,1,1,1.25,1,1,1,1,"test"};
  BHeist itest2={"atest2",1,1,1,1.25,1,1,1,1,"test"};
  BHeist vtest1={"vteset1",1,1,1,1.25,1,1,1,1,"test"};
  BHeist vtest2={"vteset2",1,1,1,1.25,1,1,1,1,"test"};
  std::vector<BHeist> a3={atest1,atest2};
  std::vector<BHeist> w3={wtest1,wtest2};
  std::vector<BHeist> i3={itest1,itest2};
  std::vector<BHeist> v3={vtest1,vtest2};
  baza.AddByValue(vtest);
  baza.AddByAddress(atest.address, atest);
  baza.AddByWeight(wtest.w, wtest);
  baza.AddByIncome(itest.income, itest);
  baza.AddByValues(v3);
  std::vector<std::string> aa;
  std::vector<double> ww;
  std::vector<int> ii;
  for(int i=0;i<2;i++){
    aa.push_back(a3[i].address);
  }
  for(int i=0;i<2;i++){
    ww.push_back(w3[i].w);
  }
  for(int i=0;i<2;i++){
    ii.push_back(i3[i].income);
  }
  baza.AddByIncomes(ii,i3);
  baza.AddByWeights(ww,w3);
  baza.AddByAddresses(aa,a3);
  //SEARCH
  baza.searchAddress1("atest");
  baza.searchAddress2(aa);
  baza.searchAddressValue(atest);
  baza.searchAddressValues(a3);
  baza.searchWeight1(1);
  baza.searchWeight2({1,2});
  baza.searchWeightValue(wtest);
  baza.searchAddressValues(w3);
  baza.searchIncome1(1);
  baza.searchIncome2(ii);
  baza.searchIncomeValue(itest);
  baza.searchIncomeValues(i3);
  //DELETE
  baza.deleteAddress("atest");
  baza.deleteWeight(baza.ReturnMaxWeight1a());
  baza.deleteIncome(baza.ReturnMaxIncome1a());
  baza.deleteAddresses(aa);
  baza.deleteIncomes({2147473799,2147336076});
  baza.deleteWeights(baza.ReturnMaxWeight2a(5));
  baza.searchAddress2(aa);
}